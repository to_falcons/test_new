<x-layout>
	<x-slot name="title">
		Home | Feed
	</x-slot>
	<x-slot name="header">
		Welcome
	</x-slot>
		<div id="feed">
			<p>
				It's a simple page intended for collecting short messages. No option to delete your message, so think before you post!
			</p>
			<h2>Please enter your message below</h2>
			<form method="post" id="message_form" action="send">
				<textarea rows="5" name="post" id="message_field" value="Your text here"></textarea>
				<input type="submit" name="send" action="send" id="message_submit" value="Send!"/>
			</form>
			<h2>What others say...</h2>
			@for ($i = 0; $i < count($posts); $i++)
				@if ($recent_post == 1 & $i == 0)
					<p id="fd_date_recent">{{$posts[$i]->created_at}}</p><p id="fd_text_recent">{{$posts[$i]->text}}</p>
					@continue
				@endif
				<p class="fd_date">{{$posts[$i]->created_at}}</p><p class="fd_text">{{$posts[$i]->text}}</p>
			@endfor
		</div>
</x-layout>