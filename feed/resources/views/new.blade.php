<x-layout>
	<x-slot name="title">
		Errors | Feed
	</x-slot>
	<x-slot name="header">
		Something is wrong
	</x-slot>
		@if ($errors->any())
			<div class="alert">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
				<p>Please click <a href="/">here</a> to return to the previous page</p>
			</div>
		@endif
</x-layout>