<!DOCTYPE html>

<HTML lang="en-GB">
	<head>
		<meta charset="utf-8" name="viewport" content="width=device-width">
		<title>{{ $title ?? '... | Feed' }}</title>
		<link href="/css/app.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<h1>{{ $header }}</h1>
		<div id="border">
			{{ $slot }}
		</div>
	</body>
</HTML>