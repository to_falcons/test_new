<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Post;

class Controller extends BaseController
{
	public function index(Request $request) {
		// Default view
		$posts = Post::orderBy('created_at','desc')->get()->all();

		if (empty($posts)) {
			throw new \Illuminate\Database\Eloquent\ModelNotFoundException();
		}
		return view('index', ['recent_post' => 0], ['posts' => $posts]);
	}

	public function send(Request $request) {
		// Validate request
		$validator = Validator::make($request->all(), ['post' => 'required|max:120',]);

		if ($validator->fails()) {
			return view('new', ['errors' => $validator->errors()]);
		}

		// Submitted form view
		if ($request->post != NULL) {
			$post = new Post;
			$post->text = $request->post;
			$post->save();

			$posts = Post::orderBy('created_at','desc')->get()->all();

			return view('index', ['recent_post' => 1], ['posts' => $posts]);
		}
	}
}