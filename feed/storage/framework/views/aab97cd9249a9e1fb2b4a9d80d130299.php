<!DOCTYPE html>

<HTML lang="en-GB">
	<head>
		<meta charset="utf-8" name="viewport" content="width=device-width">
		<title><?php echo e($title ?? '... | Feed'); ?></title>
		<link href="/css/app.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<h1><?php echo e($header); ?></h1>
		<div id="border">
			<?php echo e($slot); ?>

		</div>
	</body>
</HTML><?php /**PATH C:\Users\Acer\feed\resources\views/components/layout.blade.php ENDPATH**/ ?>