<?php if (isset($component)) { $__componentOriginal71c6471fa76ce19017edc287b6f4508c = $component; } ?>
<?php $component = Illuminate\View\AnonymousComponent::resolve(['view' => 'components.layout','data' => []] + (isset($attributes) && $attributes instanceof Illuminate\View\ComponentAttributeBag ? (array) $attributes->getIterator() : [])); ?>
<?php $component->withName('layout'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php if (isset($attributes) && $attributes instanceof Illuminate\View\ComponentAttributeBag && $constructor = (new ReflectionClass(Illuminate\View\AnonymousComponent::class))->getConstructor()): ?>
<?php $attributes = $attributes->except(collect($constructor->getParameters())->map->getName()->all()); ?>
<?php endif; ?>
<?php $component->withAttributes([]); ?>
	 <?php $__env->slot('title', null, []); ?> 
		Home | Feed
	 <?php $__env->endSlot(); ?>
	 <?php $__env->slot('header', null, []); ?> 
		Welcome
	 <?php $__env->endSlot(); ?>
		<div id="feed">
			<p>
				It's a simple page intended for collecting short messages. No option to delete your message, so think before you post!
			</p>
			<h2>Please enter your message below</h2>
			<form method="post" id="message_form" action="send">
				<textarea rows="5" name="post" id="message_field" value="Your text here"></textarea>
				<input type="submit" name="send" action="send" id="message_submit" value="Send!"/>
			</form>
			<h2>What others say...</h2>
			<?php for($i = 0; $i < count($posts); $i++): ?>
				<?php if($recent_post == 1 & $i == 0): ?>
					<p id="fd_date_recent"><?php echo e($posts[$i]->created_at); ?></p><p id="fd_text_recent"><?php echo e($posts[$i]->text); ?></p>
					<?php continue; ?>
				<?php endif; ?>
				<p class="fd_date"><?php echo e($posts[$i]->created_at); ?></p><p class="fd_text"><?php echo e($posts[$i]->text); ?></p>
			<?php endfor; ?>
		</div>
 <?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php if (isset($__componentOriginal71c6471fa76ce19017edc287b6f4508c)): ?>
<?php $component = $__componentOriginal71c6471fa76ce19017edc287b6f4508c; ?>
<?php unset($__componentOriginal71c6471fa76ce19017edc287b6f4508c); ?>
<?php endif; ?><?php /**PATH C:\Users\Acer\feed\resources\views/index.blade.php ENDPATH**/ ?>