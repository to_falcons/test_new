<?php

namespace Database\Seeders;

use App\Models\Post;
use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    public function run()
    {
        Post::create([
            'text' => 'Success is going from failure to failure without losing your enthusiasm'
        ]);

        Post::create([
            'text' => 'Dream big and dare to fail'
        ]);

        Post::create([
            'text' => 'It does not matter how slowly you go as long as you do not stop'
        ]);

        //... add more quotes if you want!
    }
}